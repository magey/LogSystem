import axios from '@/libs/api.request';

const controller = 'api/CommonData/';

/**
 * 获取所有日志级别
 * @constructor
 */
export const QueryLogLevels = () => {
    return axios.request({
        url: controller + 'QueryLogLevels',
        method: 'get'
    });
};

/**
 * 获取所有平台
 * @constructor
 */
export const QueryPlatforms = () => {
    return axios.request({
        url: controller + 'QueryPlatforms',
        method: 'get'
    });
};
