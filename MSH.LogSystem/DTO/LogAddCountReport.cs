﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    /// <summary>
    /// 日志统计报表
    /// </summary>
    public class LogAddCountReport
    {
        /// <summary>
        /// 今日新增日志总量
        /// </summary>
        public long TodayCount
        {
            get
            {
                return TodayInfoCount + TodayDebugCount + TodayWarnCount + TodayErrorCount;
            }
        }

        /// <summary>
        /// 今日新增Info日志
        /// </summary>
        public long TodayInfoCount { get; set; }

        /// <summary>
        /// 今日新增Debug日志
        /// </summary>
        public long TodayDebugCount { get; set; }

        /// <summary>
        /// 今日新增Warn日志
        /// </summary>
        public long TodayWarnCount { get; set; }

        /// <summary>
        /// 今日新增Error日志
        /// </summary>
        public long TodayErrorCount { get; set; }
    }
}
