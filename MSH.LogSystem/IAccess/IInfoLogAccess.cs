﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAccess
{
    public interface IInfoLogAccess
    {
        void AddLog(LogRequest logRequest);

        void AddLog(List<LogRequest> logRequest);

        List<LogInfo> QueryLogRequest(LogQuery query);

        long GetTotalCount();

        long GetTodayCount();

        void DeleteLog(string id);

        void DeleteLog(List<string> ids);
    }
}
