﻿using System;
using System.Diagnostics;
using System.Linq;
using LiteDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LiteDbTest
{
    [TestClass]
    public class 测试读取
    {
        [TestMethod]
        public void 读取全部()
        {
            var watch = new Stopwatch();
            watch.Start();
            // Open database (or create if not exits)
            using (var db = new LiteDatabase("LogObject.db"))
            {
                // Get customer collection
                var collection = db.GetCollection<LogObject>();

                // Insert new customer document (Id will be auto-incremented)
                var res = collection.FindAll().ToList();
                Console.WriteLine($"总条数:{res.Count}");
            }
            watch.Stop();
            Console.WriteLine($"单条执行时间:{watch.ElapsedMilliseconds}毫秒");
        }

        [TestMethod]
        public void 读取单条()
        {
            var watch = new Stopwatch();
            watch.Start();
            // Open database (or create if not exits)
            using (var db = new LiteDatabase("LogObject.db"))
            {
                // Get customer collection
                var collection = db.GetCollection<LogObject>();

                var maxId = collection.Max(a => a.Id).AsInt64;
                // Insert new customer document (Id will be auto-incremented)
                var res = collection.FindOne(a => a.Id == maxId);
            }
            watch.Stop();
            Console.WriteLine($"读取单条执行时间:{watch.ElapsedMilliseconds}毫秒");
        }
    }
}
